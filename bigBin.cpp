#include <cmath>
#include <iostream>
#include <ios>

//{\binom {6n}{n}}(\frac{1}{6})^n(\frac{5}{6})^{5n}=\prod _{i=1}^{n}{\frac {6n+1-i}{i}}\cdot(\frac{1}{6})^n(\frac{5}{6})^{5n}=\prod _{i=1}^{n}({\frac {6n+1-i}{i}}\cdot(\frac{1}{6})\cdot(\frac{5}{6})^{5})=\prod _{i=1}^{n}({\frac {6n+1-i}{i}}\cdot0.067)
typedef long long vlong; 

inline double body(vlong y, vlong x, vlong i, double mult)
{
	return mult*(y-i)/6.0/double(x-i);
}

double getMult(vlong tosses, vlong ones) {
	const vlong powerConst = (tosses-ones)/ones;
	const double powBase = 5.0/6.0;
	const double mult = std::pow(powBase, powerConst);
    return mult;
}

double bigBin(vlong tosses, vlong ones)
{
    if(tosses<ones) {
        std::cerr << "tosses<ones!\n";
        return -1.0;
    }
	if((tosses-ones) % ones) {
        std::cerr << "Not 0 remainder!\n";
        return -2.0;
    }

	const double mult = getMult(tosses,ones);    
	double res = 1.0;

	vlong head = 0;
	vlong tail = ones-1;

	double headFact = body(tosses,ones, head, mult);
	double tailFact = body(tosses,ones, tail, mult);

	while(1)
	{
		double headResult = headFact*res;
		double tailResult = tailFact*res;
		double headSize = headResult < 1.0 ? 1.0/headResult : headResult;
		double tailSize = tailResult < 1.0 ? 1.0/tailResult : tailResult;
        
		if ( headSize < tailSize)
		{
           res = headResult;
		   ++head;
           headFact = body(tosses,ones, head, mult);
		}
		else
		{
		    res = tailResult;
            --tail;
            tailFact = body(tosses,ones, tail, mult);
		}
        
        if(head > tail)
            break;
	}
	return res;
}


int main(){
	vlong tosses(6000000000);
	vlong ones(1000000000);

    double res = bigBin(tosses,ones);
    std::cout << res << ' ' << std::scientific << res << std::endl;

	return 0;
}
